from unittest import TestCase

from sistema_vendas.calculadora_comissao import CalculadoraComissao


class TestCalculadoraComissao(TestCase):
    def test_vendamenor10k_vendadecemreais(self):
        valor_venda = 100
        resultado = 5
        comissao = CalculadoraComissao.calcular(valor_venda)

        self.assertEqual(comissao, resultado)

    def test_vendamaior10k_vendadeonzemil(self):
        valor_venda = 11000
        resultado = 660

        comissao = CalculadoraComissao.calcular(valor_venda)
        self.assertEqual(resultado, comissao)

    def test_vendamenorigual10k_vendade10k(self):
        valor_venda = 10000
        resultado = 500

        comissao = CalculadoraComissao.calcular(valor_venda)
        self.assertEqual(resultado, comissao)

    def test_vendamaior10k_vendadedozemil(self):
        valor_venda = 12000
        resultado = 720

        comissao = CalculadoraComissao.calcular(valor_venda)
        self.assertEqual(resultado, comissao)

    def test_vendamenor10k_vendadecemum(self):
        valor_venda = 55.59
        resultado = 2.77

        comissao = CalculadoraComissao.calcular(valor_venda)
        self.assertEqual(resultado, comissao)

    def test_vendamaior10k_venda10keum(self):
        valor_venda = 10001.57
        resultado = 600.09

        comissao = CalculadoraComissao.calcular(valor_venda)
        self.assertEqual(resultado, comissao)
